﻿using System;
using Npgsql; 
using System.Data;

namespace vaseis1_client_2016
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			using (var conn = new NpgsqlConnection ("Host=83.212.99.115;Username=bikestorian;Password=m9zSaFszrT!;Database=BikeStore")) {
				conn.Open ();
				using (var cmd = new NpgsqlCommand()){
					cmd.Connection = conn;
					
					cmd.CommandText = "select customerid,count (distinct salesorderid) from(\n   select customerid,salesorderid from( " +
						"select salesorderid,customerid,count(distinct productcategoryid) " +
						"from(select salesorderid, customerid,count(*),productcategoryid from(\n" +
						"select salesorderheader.customerid, salesorderdetail.salesorderid, product.productid, product.productcategoryid " +
						"from salesorderheader  inner join salesorderdetail  on salesorderdetail.salesorderid = salesorderheader.salesorderid " +
						"inner join product on salesorderdetail.productid = product.productid) as b " +
						"group by customerid,salesorderid,productcategoryid)as c " +
						"group by customerid,salesorderid) as d where count>=3) as f " +
						"group by customerid\norder by customerid;";
					using (var reader = cmd.ExecuteReader ()) {
						Console.WriteLine ("CustomerID:Count(SalesOrderId)");
						while (reader.Read())
						{
							for (int i = 0; i<2; i++){
								Console.Write(reader.GetString(i));
								if (i != 1) {
									Console.Write (":");
								}
							}
							Console.WriteLine ();
						}
					}
                    cmd.CommandText = "select sum, customerid\nfrom (select customerid , sum(totaldue) \n" +
                       "from salesorderheader  \n" +
                       "group by customerid \norder by sum(totaldue) desc) as f\nwhere sum=(select max(sum) \n" +
                       "from (select customerid , sum(totaldue) \nfrom salesorderheader " +
                       "\ngroup by customerid \norder by sum(totaldue) desc) as f2);";
                    using (var reader = cmd.ExecuteReader())
                    {
                        Console.WriteLine("Sum:CustomerID");
                        while (reader.Read())
                        {

                            for (int i = 0; i < 2; i++)
                            {
                                Console.Write(reader.GetString(i));
                                if (i != 1)
                                {
                                    Console.Write(":");
                                }
                            }
                            Console.WriteLine();
                        }
                    }
                }

			}
            Console.ReadLine();
        }
	}
}
