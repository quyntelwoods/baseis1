from sqlalchemy import create_engine, select
from sqlalchemy.sql import text

#bikestorian m9zSaFszrT!

class BikeStoreQueries(object):
    def __init__(self):
        engine = create_engine("postgresql://bikestorian:m9zSaFszrT!@83.212.99.115/BikeStore", echo=True)
        self.conn = engine.connect()

    def executeQuery1(self):
        try:
            #erwtima 1
            s1 = text(
                "select sum, customerid "
                "from 	(select customerid , sum(totaldue) "
	            "from salesorderheader "
	            "group by customerid  "
	            "order by sum(totaldue) desc) as f "
                "where sum=(select max(sum) "
		        "from (	select customerid , sum(totaldue) "
			    "from salesorderheader "
			    "group by customerid "
			    "order by sum(totaldue) desc) as f2);"
            )
            result = self.conn.execute(s1).fetchall()
        except Exception as e:
            raise

        return result

    def executeQuery2(self):
        try:
            s2 = text(
                "select customerid,count (distinct salesorderid) from( "
                "select customerid,salesorderid from( "
                "select salesorderid,customerid,count(distinct productcategoryid) from( "
                "select salesorderid, customerid,count(*),productcategoryid from( "
                "select salesorderheader.customerid, salesorderdetail.salesorderid, product.productid, product.productcategoryid "
                "from salesorderheader "
                "inner join salesorderdetail "
                "on salesorderdetail.salesorderid = salesorderheader.salesorderid "
                "inner join product "
                "on salesorderdetail.productid = product.productid) as b "
                "group by customerid,salesorderid,productcategoryid)as c "
                "group by customerid,salesorderid) as d "
                "where count>=3) as f "
                "group by customerid "
                "order by customerid;"
            )
            result = self.conn.execute(s2).fetchall()
        except Exception as e:
            raise

        return result

bsq = BikeStoreQueries()
for instance in bsq.executeQuery1():
    print (instance)
for instance in bsq.executeQuery2():
    print (instance)
